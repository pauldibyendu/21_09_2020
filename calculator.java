
import java.util.Scanner;

public class calculator {
    public static void main(String []args) {
        double a, b, result;
        char ch;

        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the first number");
        a = scan.nextDouble();

        System.out.println("Enter the Second number");
        b = scan.nextDouble();

        System.out.println("Enter which operation you want to be carried out with these numbers:-");
        System.out.println("Enter '+' for addition");
        System.out.println("Enter '-' for subtraction");
        System.out.println("Enter '*' for multiplication");
        System.out.println("Enter '/' for division");
        System.out.println("Enter '%' for remainder");
        ch = scan.next().charAt(0);

        switch(ch)
        {
            case '+' :
                result = a + b;
                System.out.println("Summation is " + result);
                break;
            case '-' :
                result = a - b;
                System.out.println("Subtraction is " + result);
                break;
            case '*' :
                result = a * b;
                System.out.println("Multiplication is " + result);
                break;
            case '/' :
                result = a / b;
                System.out.println("Division is " + result);
                break;
            case '%' :
                result = a % b;
                System.out.println("Remainder is " + result);
                break;
            default :
                System.out.println("Invalid operation");
                break;
        }
    }
}
